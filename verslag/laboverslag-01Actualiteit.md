## Laboverslag: Actualiteit

- Naam cursist: Michiel Van Der Eecken
- Bitbucket repo: https://bitbucket.org/michielVanDerEecken/enterprise-linux-labo

### Procedures

Toevoeging op taak vsftpd. Shares beschikbaar maken voor de users

1. firewall rules aanpassen

- name: Set firewall rules
  firewalld:
    service=ftp
    state=enabled
    permanent={{ item }}
  with_items:
    - true
    - false
  tags: vsftpd


 2. SELinux settings aanpassen zodat er toegang is tot ftp 

- name: SELinux Settings
  seboolean: name={{ item }}  state=yes persistent=yes
  with_items:
    - ftp_home_dir
    - ftpd_full_access
	
3. template aanpassen
{% if vsftpd_allow_anonymous is defined %}
anonymous_enable={{vsftpd_allow_anonymous}}
{% endif %}

#user acces
local_enable=YES
local_umask=022
userlist_deny=YES
write_enable=YES
guest_enable=NO

connect_from_port_20=YES
listen=YES
listen_ipv6=NO
pam_service_name=vsftpd

### Testplan en -rapport

- Test runnen -> slagen

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- De toegang tot de ftp mappen toekennen aan vsftpd

#### Wat ging niet goed?

- a. user niet toelaten
- ipv6 settings aanpassen

#### Wat heb je geleerd?

de rechten toekennen tot ftp shares

