## Laboverslag: DHCP

- Naam cursist: Michiel Van Der Eecken
- Bitbucket repo: https://bitbucket.org/michielVanDerEecken/enterprise-linux-labo

### Procedures


in hostvars/pr001.yml

1. Global variabelen declareren
dhcp_global_default_lease_time: 14400
dhcp_global_max_lease_time: 43200
dhcp_global_domain_name: Linuxlab.lan
dhcp_global_domain_name_servers:
  - 192.0.2.10
  - 192.0.2.11
dhcp_global_subnet_mask: 255.255.0.0
dhcp_global_routers: 192.0.2.254

2. range toevoegen voor vaste host ips
dhcp_subnets:
  - ip: 172.16.0.0
    netmask: 255.255.0.0
    range_begin: 172.16.10.1
    range_end: 172.16.99.255
    name: reservedIps
    default_lease_time: 43200
    max_lease_time: 43200
    domain_name_servers: 
      - '192.0.2.10'
      - '192.0.2.11'
    hosts:
      - name: testpc
        ip: 172.16.10.25
        mac: 08:00:27:0A:F7:3C



### Testplan en -rapport

1. vagrant provision
	-> alles slaagt
2. inloggen op dhcpserver
	-> systemctl status dhcpd.service -> running
3. Ubuntu installeren in virtualbox met zelfde host-only adapter als de dhcp
  -> eerste host-only: krijgt ip 172.16.10.25 (toegewezen via mac adres in hostvars)
  -> tweede host-only: krijgt ip 172.16.100.1 (eerste adres in volgende range)

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- global settings toevoegen
- de range voor de fixed host ip's toevoegen
- de testhost toevoegen

#### Wat ging niet goed?

- Ik was vergeten dat de host-only adapters in virtual box dezelfde moeten zijn bij dhcp en testpc
  daarom kreeg ik geen ip's van mijn dhcp

#### Wat heb je geleerd?

- een dhcp configureren mbv ansible

#### Waar heb je nog problemen mee?

/

### Referenties

