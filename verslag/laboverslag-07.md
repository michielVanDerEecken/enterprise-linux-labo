## Laboverslag: Troubleshoot 2

- Naam cursist: Michiel Van Der Eecken
- Bitbucket repo: https://bitbucket.org/michielVanDerEecken/enterprise-linux-labo

### Procedures

1. verander de owner van alpha en bravo naar bob
	-> chown alpha bob
2. verander de chmod van alpha, echo, charlie
	-> chmod -R

* Stap 1 en 2 ook voor echo, charlie

3. in /etc/samba/samba.conf
	-> bij bravo	-> read list @bravo, alice
					-> directory mode = 0770
	-> bij foxtrot	-> write list =@foxtrot
4. firewall toelaten
	-> sudo firewall-cmd --permanent --add-service=samba
	-> sudo systemctl restart service firewalld

	

### Testplan en -rapport

1. testen via verkenner
	-> \\192.168.56.40
2. test runnen
	-> ./runbats.sh -> alles werkt

- 

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- aanpassen adhv van chown en chmod

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

#### Wat heb je geleerd?

hoe je een samba server maakt

#### Waar heb je nog problemen mee?

de bravo share, werkt momenteel wel, maar problemen mee gehad 
### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
