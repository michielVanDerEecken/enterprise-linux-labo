## Laboverslag: SAMBA EN VSFTPD

- Naam cursist: Michiel Van Der Eecken
- Bitbucket repo: https://bitbucket.org/michielVanDerEecken/enterprise-linux-labo

### Procedures

*hostvars/pr011.yml
1. toevoegen van user groeps
el7_user_groups:
  - directie
  - financieringen
  - staf
  - verzekeringen
  - publiek
  - beheer 

2. users aanmaken
el7_users:
  - name: michiel
    comment: 'Michiel admin'
    shell: /bin/bash    
    groups: 
      - users
      - "wheel"
    password: '$6$8NRllmROCtP8GlWF$IlW/.tgSjeaoL.63CB9Y/YmccWOMMl08bXJbpNnCk/P5uR1oKQVMdHbNatk2nhzvYQAxl4M6EK.cZkOoNkBwK0'

  - name: franka
    comment: 'Frank Assengraaf'
    shell: /sbin/nologin        
    groups: 
      - directie
      - financieringen
      - staf
      - verzekeringen
    password: 'franka'
  enz...

3. samba root 
samba_share_root: /srv/shares

4.samba shares aanmaken
samba_shares:
  - name: directie
    group: directie
    comment: 'directie'        
    valid_users: michiel, franka, maaiked, femkevdv    
    write_list: michiel, franka, maaiked, femkevdv   
  - name: publiek
    group: publiek
    comment: 'publiek'
    valid_users: michiel, franka, maaiked, femkevdv, hansb, kimberlyvh, taniav, peterj   
    write_list: michiel, franka, maaiked, femkevdv, hansb, kimberlyvh, taniav, peterj  
  - name: verzekeringen
    group: verzekeringen
    comment: 'verzekeringen'   
    valid_users: michiel, franka, maaiked, hansb, kimberlyvh, taniav, femkevdv, peterj  
    write_list: michiel, franka, maaiked, hansb, kimberlyvh, taniav  
  - name: staf
    group: staf
    comment: 'staf'
    valid_users: michiel, franka, maaiked, hansb, kimberlyvh, taniav, femkevdv, peterj 
    write_list: michiel, maaiked, femkevdv, franka   
  - name: financieringen
    group: financieringen
    comment: 'financieringen'
    valid_users: michiel, franka, maaiked, hansb, kimberlyvh, taniav, femkevdv, peterj 
    write_list: michiel, maaiked, franka, peterj

    enz...

5. andere
samba_map_to_guest: Never   

samba_netbios_name: FILES

samba_create_varwww_symlinks: yes

samba_load_homes: true

vsftpd_netbios_name: VSFTPD 
vsftpd_allow_anonymous: NO

VSFTP SERVER

1. Aanmaken van vsftpd.conf.j2 in /roles/templates

in main.yml
2. installeren van de service
- name: Installing packages
  yum:
    pkg: "{{ item }}"
    state: present
  with_items:
    - vsftpd
  tags:
    - vsftpd

3. service starten
- name: Ensure service is started
  service:
    name: "{{ item }}"
    state: started
    enabled: true
  with_items:
    - vsftpd
  tags: vsftpd

4. De config file installeren
- name: Install Vsftpd config file
  template:
    src: vsftpd.conf.j2
    dest: /etc/vsftpd/vsftpd.conf
  tags: vsftpd

5. De firewall instellen
- name: Set firewall rules
  firewalld:
    service=ftp
    state=enabled
    permanent={{ item }}
  with_items:
    - true
    - false
  tags: vsftpd

*Dit is enkel omdat het niet werkt de config file
7. Anonymous user niet toelaten
- name: don't allow anonymous users
  replace:
    dest: /etc/vsftpd/vsftpd.conf
    regexp: 'anonymous_enable=YES'
    replace: 'anonymous_enable=NO'

in vsftp.conf.j2



8. global toevoegen en ansible managed toevoegen
# {{ ansible_managed }}
[global]

9. 	anonymous users niet toelaten
anonymous_enable={{vsftpd_allow_anonymous}}

10. netbiosnaam instellen
netbios name = {% if vsftpd_netbios_name is defined %}{{ vsftpd_netbios_name }}{% else %}{{ ansible_hostname }}{% endif %}

### Testplan en -rapport

- vagrant destroy en vagrant up 
    -> werkt
- vagrant provision
    -> alles ok
- in de files gaan via hostsysteem via \\files
    - aanmelden met mijn gebruikersnaam en wachtwoord
    -> lukt
- inloggen op vsftpd server via ftp://172.16.0.11
- inloggen via met verschillende gebruikers
    -> lukt
- runbats runnen
  -> samba is foutloos
  -> vsftpd foutloos tot  ✗ Anonymous user should not be able to see shares

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- samba

#### Wat ging niet goed?

 Anonymous users niet toelaten

#### Wat heb je geleerd?

de vsftpd service

#### Waar heb je nog problemen mee?

de anonymous users blokkeren, vsftpd shares toevoegen

### Referenties

https://github.com/ayn/ansible-playbooks
https://www.centos.org/docs/5/html/Deployment_Guide-en-US/s1-ftp-vsftpd-conf.html
