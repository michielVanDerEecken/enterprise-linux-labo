## Laboverslag: DNS EN BIND

- Naam cursist: Michiel Van Der Eecken
- Bitbucket repo: https://bitbucket.org/michielVanDerEecken/enterprise-linux-labo

### Procedures
1. pu001 en pu002 aanmaken in site.yml en in vagrant hosts.yml. 
  Deze hierin ook het juiste ip geven, 192.0.2.10 en 192.0.2.11(slave). Ook toekennen van roles: bind en el7

2. Aanmaken yml file in host var van pu001 en pu002
3. In pu001 de bind zone host ingeven (master en slave):
bind_zone_hosts:
  - name: pu001
    ip: 192.0.2.10
    aliases: 
      - ns1

  - name: pu002
    ip: 192.0.2.11
    aliases:
      - ns2
4. Master aanduiden
bind_zone_master_server_ip: 192.0.2.10

5. Bind zone, netwerkdomein
Bind_zone_name: 'linuxlab.lan'

6. ipv4 en ipv6 toelaten en dns laten luisteren naar ipv4 en ipv6
bind_allow_query:
  - 'any'

bind_listen_ipv4:
  - 'any'

bind_listen_ipv6:
  - 'any'


#in host_vars/pu002.yaml

7. Bind name servers definieren 
bind_zone_name_servers:
  - 'pu001'
  - 'pu002' 

8. master definieren
bind_zone_master_server_ip: '192.0.2.10'

9. domeinnaam opgeven
bind_zone_name: 'linuxlab.lan'

10. dns aanvragen, ipv4 en ipv6 toestaan
 bind_allow_query:
  - 'any'

bind_listen_ipv4:
  - 'any'

bind_listen_ipv6:
  - 'any'

11. Master record tussen dns servers laten kopieren naar slave
bind_allow_transfer:
  - '192.0.2.10'
  - '192.0.2.11'

12. filter plugins kopieren naar ansible home

13. toevoegen aan pu001.yaml
- name: 'pu004'
    ip: '192.0.2.50'
    aliases:
      - www

  - name: 'pu003'
    ip: '192.0.2.20'
    aliases:
      - mail

  - name: 'pr001'
    ip: '172.16.0.1'
    aliases:
      - dhcp

  - name: 'pr002'
    ip: '172.16.0.2'   
    aliases:
      - directory

  - name: 'pr010'
    ip: '172.16.0.10'
    aliases:
      - inside

  - name: 'pr011'
    ip: '172.16.0.11'
    aliases:
      - files

14. mail server voor domein aanduiden
bind_zone_mail_servers: 
  - name: 'pu003'
    preference: '10'


15. group vars aanpassen voor firewall, allow dns

### Testplan en -rapport

1. vagrant provision, alles taken slagen
2. nslookup 
     - nslookup linuxlab.lan 192.0.2.10
     - nslookup linuxlab.lan 192.0.2.11
3. testen in pu001
     - vagrant ssh pu001
     - sudo /vagrant/test/runbats.sh
         - alle testen slagen
4. testen in pu002
     - vagrant ssh pu002
     - sudo /vagrant/test/runbats.sh

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- bind zone host toevoegen
- master dns toevoegen

#### Wat ging niet goed?

- had enkele spaties vergeten waardoor ik steeds errors kreeg en ik vond de fout niet.
- mail server vergeten toevoegen

#### Wat heb je geleerd?

hoe ik de variabelen in host vars kan declareren

#### Waar heb je nog problemen mee?

niets

### Referenties

bitbucket en github repo's