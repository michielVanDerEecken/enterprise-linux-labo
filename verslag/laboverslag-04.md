## Laboverslag: Troubleshoot 004

- Naam cursist: Michiel Van Der Eecken
- Bitbucket repo: https://bitbucket.org/michielVanDerEecken/enterprise-linux-labo

### Procedures

- in/etc/named.conf
	- ip's aanpassen
	- 127.0.0.0 veranderen naar "any"

- in /etc/sysconfig/../ifcfg-enp0s8 
	- onboot naar yes veranderen

- set enp0s van down naar up
	- sudo ip link set dev enp0s8 up 

- '.' toevoegen na de namen
- in /var/cynalco.com 
	- beetle & butterfree (12 & 13 omwisselen)

- /var/named/cynalco.com
	- Nieuwe NS toevoegen "tamatama"  
	- Mankey alias 'files' met CNAME

- mankey files toevoegen CNAME

- firewall instellingen aanpassen, dns toestaan
	- sudo firewall-cmd --add-service=dns 


	
### Testplan en -rapport
- inloggen met ssh
- commando dig gebruiken om manueel aan te roepen
	dig @192.168.56.42 beetle.cynalco.com
- de bats file runnen


### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- nieuwe NS toevoegen
- toevoegen aan firewall
- mankey alias toevoegen


#### Wat ging niet goed?

- de fouten in de domeinnamen vinden, '.' toevoegen
- ip's aanpassen

#### Wat heb je geleerd?

- Hoe ik een dns-server kan controleren op fouten
- In welke bestanden ik op zoek moet gaan naar fouten
- Commando's waarmee ik de status en de oorzaak van de fout kan achterhalen 

#### Waar heb je nog problemen mee?

/
### Referenties

/
