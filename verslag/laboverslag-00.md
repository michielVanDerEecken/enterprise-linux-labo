## Laboverslag: TITEL

- Naam cursist: Michiel Van Der Eecken
- Bitbucket repo: https://bitbucket.org/michielVanDerEecken/enterprise-linux-labo

### Procedures

1. Account aanmaken op bitbucket, repo forken
2. Nieuwste versie Vagrant en Github downloaden en installeren
3. Repo clonen en downloaden
4. Aanpassen group_vars/all.yaml
	a) pkg installeren
	b) aanmaken user
	c) user toevoegen aan admins
5. Vagrant up
6. inloggen vanop hostsysteem met sshkey

### Testplan en -rapport

-
1) Repository bekijken met commando "ls -a" om te verifieren of de repo gecloned is.
2) Vagrant up gebruiken, kijken of dat werkt. 
3) Vagrant provision gebruiken om te kijken of er iets faalt of alle taken slagen.
4) runbats.sh uitvoeren
5) inloggen op de server met vagrant sh

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- ...
- Account aanmaken op bitbucket en repo forken
- Githud en Vagrant downloaden en installeren
- inloggen op de server
- runbats.sh uitvoeren

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?
-Wat ik juist moest invullen in group_vars wist ik niet volledig. Maar na even opzoeken hebben ik het gevonden.

#### Wat heb je geleerd?

Group vars "all" aanvullen zodat er packages worden geinstalleerd.

#### Waar heb je nog problemen mee?

//


### Referenties

- Hoe je ssh key moet aanmaken.
https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html

-Key hashen 
https://www.mkpasswd.net/
