## Laboverslag: LAMP

- Naam cursist: Michiel Van Der Eecken
- Bitbucket repo: https://bitbucket.org/michielVanDerEecken/enterprise-linux-labo

### Procedures

1) Install common packages toevoegen aan playbook
	- deze installeert mariadb, mariadbserver en MysQL-Pyhton
2) Task toevoegen om mariadb service te starten
3) database aanmaken met variabelen gedeclareerd in de vars
4) database user aanmaken (ook met vars)
5) Apache installeren met;
	-httpd
	-php
	-php-xml
	-php-mysql
6) firewalld enablen
7) firewall configureren voor zowel http als https
8) unzip installeren
9) wordpress unzippen en installeren
	

### Testplan en -rapport

1) vargrant up
		- kijken of alles geinstalleerd is
		- nagaan of wordpress pagina tevoorschijn komt
		- vagrant provision
		- runbats.sh runnen

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Alles tot en met het installeren van wordpress

#### Wat ging niet goed?

- Https-ondersteuning met self-signed certificate

#### Wat heb je geleerd?

- ik kende dit nog van vorig jaar, maar het was een goede opfrissen voor het werken met playbooks.

#### Waar heb je nog problemen mee?

- Https-ondersteuning met self-signed certificate

### Referenties

redhat system admins guide

